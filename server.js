const express = require('express'),
    nunjucks = require('nunjucks'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    cookieParser = require('cookie-parser'),
    flash = require('connect-flash'),
    app = express();

let login = require('./config').dbLogin,
    pass = require('./config').dbPass,
    address = require('./config').dbAddress,
    dbName = require('./config').dbName,
    dbPort = require('./config').dbPort,
    url = 'mongodb://'+address+':'+dbPort + '/' + dbName;
    // url = 'mongodb://'+login+':'+pass+'@'+adress+':'+dbPort + '/' + dbName;
    //url = 'mongodb://localhost:27017/ExampleDB';

mongoose.Promise = global.Promise;
mongoose.connect(url, {useMongoClient: true});
module.exports.mongoose = mongoose;

app.use(
    express.static(__dirname + '/src'),
    bodyParser(),
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    cookieParser(),
    flash()
);

nunjucks.configure(__dirname + '/src/view', {
    autoescape: true,
    cache: false,
    express: app
});

require('./app/models/_events').listenEvents();

app.use('/', require('./router'));
app.listen(require('./config.js').port);

console.log('Server started!');