const mongoose = require('./../../server').mongoose;
const eth = require('./../models/eth');

let fightSchema = mongoose.Schema({
    date: Date,
    block: Number,
    firstUser: String,//address
    secondUser: String,//address
    firstBirdId: Number,
    secondBirdId: Number,
    draw: Boolean,//ничья?
    winner: String
});

fightSchema.statics = {
    /*
    * Вывод весь список боев юзера по адресу
    */
    getUserFights: async (address) => {
        return await this.find({firstUser: address});
    },
    /*
    * Топ "N" юзеров по победам, за "day"
    */
    getTopUsers: async (N, day) => { 
        let testDate = new Date(),
        lastFights = await mongoose.model('fights', fightSchema).find({
            date: {
                $gte: new Date(testDate.getTime() - day*86400000).toISOString()
            }
        });
        

        let topUsers = {};
        
        for (let i=0; i<lastFights.length; i++){
            if (!lastFights[i].draw) {
                if (topUsers[lastFights[i].winner] != undefined){ 
                    topUsers[lastFights[i].winner]++;
                }
                else{
                    topUsers[lastFights[i].winner]=1;
                }   
            }
        }

        //FIXME: оптимизировать
        //сортировка

        let keys = Object.keys(topUsers);
        for (let i=0; i<keys.length; i++){
            for (let j=i; j<keys.length; j++){
                if (topUsers[keys[i]] < topUsers[keys[j]]){
                    let buf = topUsers[keys[i]];
                    topUsers[keys[i]] = topUsers[keys[j]];
                    topUsers[keys[j]] = buf;
                }
            }
        }

        for (key in topUsers){
            let userData = await eth.getUserData(key);
            topUsers[key] = `${userData[1]} - ${topUsers[key]}`;
        }

        return Object.entries(topUsers);
    },
    /**
     * Последние "nums" битв
     */
    getLastFights: async (nums) => {
        //console.log(this);
        return await mongoose.model('fight', fightSchema).find({}).sort({date: -1}).limit(nums); 
    }
}

fightSchema.pre('save', async function(next){
    let _fight = await mongoose.model('fight', fightSchema).find({block: this.block});
    if (_fight.length == 0){
        
        next();
    }
    else
        return;
});

// модель данных и ее экспорт
let fightModel = mongoose.model('fight', fightSchema);
module.exports.fightModel = fightModel;