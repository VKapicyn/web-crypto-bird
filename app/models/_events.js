const config = require('../../config'),
    fightModel = require('./fights').fightModel,
    userModel = require('./user').userModel;

let Web3 = require('web3'),
web3 = new Web3(new Web3.providers.HttpProvider(config.provider));

let coreCont = web3.eth.contract(config.coreABI),
    instanceCont = coreCont.at(config.coreCont),
    BasketPurchases = instanceCont.BasketPurchases(),
    birdLvlUp = instanceCont.birdLvlUp(),
    OpenBasket = instanceCont.OpenBasket(),
    fightResult = instanceCont.fightResult();

exports.listenEvents = () => {
    
    fightResult.watch(async (error, result) => {
        let winner = result.args.win.c[0],
        looser = result.args.loose.c[0],
        time = result.args.time.c[0],
        draw = result.args.draw;
        
        let fight = new fightModel();
        fight.date = new Date,
        fight.block = time,
        fight.firstUser = await instanceCont.getUserByBirdId(winner)
        fight.secondUser = await instanceCont.getUserByBirdId(looser),
        fight.firstBirdId = winner,
        fight.secondBirdId = looser,
        fight.draw = draw,
        fight.winner = draw ? null : await instanceCont.getUserByBirdId(winner)
        await fight.save();

        let userWinner = await userModel.getUser(fight.firstUser),
            userLooser;    

        if (!draw) {
            userWinner.addWinFight(fight._id),
            userLooser = await userModel.getUser(fight.secondUser);
            userLooser.addLooseFight(fight._id);
        }

        //ачивка число побед
        switch (userWinner.win.length){
            case 5:
                userWinner.setAchiv(7);
                break;
            case 15:
                userWinner.setAchiv(8);
                break;
            case 50:
                userWinner.setAchiv(9);
                break;
            case 250:
                userWinner.setAchiv(10);
                break;
            case 500:
                userWinner.setAchiv(11);
                break;
        }

        //ачивка на число боев
        switch (userWinner.win.length + userWinner.loose.length) {
            case 25:
                userWinner.setAchiv(29);
                break;
            case 100:
                userWinner.setAchiv(30);
                break;
        }

        switch (userLooser.win.length + userLooser.loose.length) {
            case 25:
                userLooser.setAchiv(29);
                break;
            case 100:
                userLooser.setAchiv(30);
                break;
        }

        userWinner.save();
        userLooser.save();
    });

    birdLvlUp.watch(async (error, result) => {
        console.log(result);
        let level = result.args.n.c[0],
            user = result.args.user.c[0];
        
        user = await userModel.getUser(user);

        switch(level){
            case 4: 
                user.setAchiv(26);
                break;
            case 10: 
                user.setAchiv(24);
                break;
            case 11:
                user.setAchiv(27);
                break; 
            case 20: 
                user.setAchiv(25);
                break;
            case 21: 
                user.setAchiv(28);
                break;
        }
    });

    BasketPurchases.watch(async (error, result) => {
        console.log(result);
        let bought = result.args.n.c[0],
            user = result.args.user.c[0];

        user = await userModel.getUser(user);

        switch(bought){
            case 1: 
                user.setAchiv(12);
                break;
            case 10: 
                user.setAchiv(13);
                break;
            case 50:
                user.setAchiv(14);
                break; 
            case 300: 
                user.setAchiv(15);
                break;
            }
    });

    OpenBasket.watch(async (error, result) => {
        console.log(result);
        let user = result.args.user.c[0];

            user = await userModel.getUser(user);

        let birds = await eth.getNumUserUnicBirds(user);

        switch(birds) {
            case 5: 
                user.setAchiv(1);
                break;
            case 10:
                user.setAchiv(2);
                break;
            case 15:
                user.setAchiv(3);
                break;
            case 20:
                user.setAchiv(4);
                break;
            case 25:
                user.setAchiv(5);
                break;
            case 30:
                user.setAchiv(6);
                break;
        }
    });
}

//добавить проверку на число разных птиц при покупках на бирже 1-6

// 16-19 покупки на бирже

// 20-23 продажи на бирже