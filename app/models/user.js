const mongoose = require('./../../server').mongoose,
    fightModel = require('./../models/fights').fightModel;

let userSchema = mongoose.Schema({
    address: {
        type: String
    },
    achiev: [
        //от 1 до 31 в соответствии с сеттингом
    ],
    win: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'fight'
    }],
    loose: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'fight'
    }]
});

userSchema.methods = {
    addWinFight: function(fight){
        this.win.push(fight);
    },
    addLooseFight: function(fight){
        this.loose.push(fight);
    },
    setAchiv: function(achiev){
        let flag = false;
        for(let i=0; i<this.achiev.length; i++){
            if (this.achiev[i] == achiev){
                flag = true;
                break;
            }
        }

        if (!flag) 
            this.achiev.push(achiev);
    },
    getLastFights: async function (num) {
        //await this.populate('fight');
        //FIXME: костыль
        let _l = [];
        for (let i=0; i<this.loose.length; i++){
            _l.push(await fightModel.findOne(this.loose[i]));
        }
        this.loose = _l;        
        let _w = [];
        for (let i=0; i<this.win.length; i++){
            _w.push(await fightModel.findOne(this.win[i]));
        }
        this.win= _w;

         
        let fights = [];
        let _loose = 1, wins = 1, max = this.loose.length + this.win.length;
        while(fights.length<num || ((max<num) && fights.length<max)){
            if (this.loose.length-_loose>=0 && this.win.length-wins>=0){
                if (this.loose[this.loose.length-_loose].date >= this.win[this.win.length-wins].date) {
                    fights.push(this.loose[this.loose.length-_loose]);
                    ++_loose;
                } else {
                    fights.push(this.win[this.win.length-wins]);
                    ++wins;
                }
            } else if (this.win.length-wins>=0 && this.loose.length-_loose<0) {
                fights.push(this.win[this.win.length-wins]);
                ++wins;
            } else if (this.win.length-wins<0 && this.loose.length-_loose>=0) {
                fights.push(this.loose[this.loose.length-_loose]);
                ++_loose;
            } else if (this.win.length-wins<0 && this.loose.length-_loose<0)
                break;
        }

        return fights;
    }
}

userSchema.statics = {
    getUser: async function(address){
        let users = await this.find({address: address});
        if (users.length == 0) {
            let user = new this();
            user.address = address;
            await user.save();
            return user;
        }
        else return users[0];
    },
    getAchives: async function(userAddress){
        let user = await this.findOne({address: userAddress});
        return user.achiev;
    }
}

// модель данных и ее экспорт
let userModel = mongoose.model('user', userSchema);
module.exports.userModel = userModel;