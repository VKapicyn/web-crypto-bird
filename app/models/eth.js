const config = require('../../config');
    
let birdModel = require('../models/bird'),
    Web3 = require('web3'),
    web3 = new Web3(new Web3.providers.HttpProvider(config.provider)),
    //web3 = new Web3(new Web3.providers.WebsocketProvider(config.provider)),
    coreContract = web3.eth.contract(config.coreABI).at(config.coreCont);//, config.coreCont),
    exchContract = web3.eth.contract(config.exchABI).at(config.exchCont);//, config.exchCont);

module.exports.coreContract = coreContract;
module.exports.exchContract = exchContract;

exports.getUserData = async (address) => {
    return await coreContract.getUserDataByAddress(address);
}

exports.getUserInventory = async (address) => {
    return await coreContract.getUserInventoryByAddress(address);
}

exports.getUserBalance = async (address) => {
    return web3.fromWei(await web3.eth.getBalance(address), 'ether').toString();
}

async function getUserBird(address) {

    let userBids = await coreContract.getUserBirdsID(address);
    let result = [];

    for (let i=0; i<userBids.length; i++){
        const id = userBids[i].c[0],
            bird = await coreContract.getBird(id),
            protect = await coreContract.getBirdStrength(id),
            attack = await coreContract.getBirdProtection(id);

        const _bird = {
                id,
                realHP: await coreContract.getRealHP(id),
                attack,
                protect, //await coreContract.getBirdProtection(i),
                level: bird[3],
                type: await coreContract.getBirdType(id),
                name: birdModel.getNameByType(await coreContract.getBirdType(id))
            }
            
            result.push(_bird);
    }

    return result;
}

module.exports.getUserBird = getUserBird;

exports.getUserEquipment = async (address) => {

    let userEquips = await coreContract.getUserEquipsID(address);
    let result = [];

    for (let i = 0; i < userEquips.length; i++) {
        let id = userEquips[i].c[0],
            equip = await coreContract.getEquip(id),
            _equip = {
                id,
                level: equip[1].toNumber(),
                type: equip[0].toNumber(),
                value: equip[2].toNumber()
            }
            
            result.push(_equip);
    }

    return result;
}

exports.getOrder = async () => {
    const ordersCount = await exchContract.getOrdersLength(),
        orders = [];

    for (let i = 0; i < ordersCount; i++) {
        const result = await exchContract.getByBirdOrdeId(i);
        // if (result.date == 0) continue;

        const birdId = result[3],
            bird = await coreContract.getBird(birdId),
            type = (await coreContract.getBirdType(birdId)).toNumber(),
            strength = (await coreContract.getBirdStrength(birdId)).toNumber(),
            protection = (await coreContract.getBirdProtection(birdId)).toNumber(),
            realHP = (await coreContract.getRealHP(birdId)).toNumber();

        orders.push({
            price: web3.fromWei(result[1].toString(), 'ether'),
            bird: {
                id: birdId,
                name: birdModel.getNameByType(type),
                lvl: bird[3],
                realHP,
                type,
                strength,
                protection
            }
        });
    }

    return orders;
}

exports.getBusketPrice = async () => {
    let result = await coreContract.getBuscketPrice();
    return [result[0], result[1], result[2]];
}

exports.getBuckets = async (address) => {
    let result = await coreContract.getBuscketsCount(address);
    return [result[0], result[1], result[2]];
}

exports.getNumUserUnicBirds = async (address) => {
    let birds = await getUserBird(address);

    uniqLength = 0;
    for(let i=0; i<birds.length; i++){
        let uniq = true;
        for(let j=0; j<birds.length; j++){
            if (j!=i && birds[j].type == birds[i].type){
                uniq = false;
                break;
            }      
        }
        if (uniq) {
            ++uniqLength;
        }
    }

    return uniqLength;
}