const coreCont = require('./eth').coreContract;
const exchCont = require('./eth').coreExch;
const fightsModel = require('./fights').fightModel;

exports.listenEvents = () => {
coreCont.events.testEvent()
    .on('data', (event) => {
        console.log(`1 ${event}`);
    })
    .on('changed', (event) => {
        console.log(`2 ${event}`);
    });

coreCont.events.testEvent((error, log) => console.log(error, log));
//console.log(coreCont.events)

/*coreCont.events.AllEvents((error, event) => {console.log(event, error)})
    .on('data', (_event) => {
        console.log(`2 ${_event}`);
    })
    .on('changed', (event) => {
        console.log(`3 ${_event}`);
    });

coreCont.watch((error, result) => {console.log(error, result)});*/

//бой (ачивки: победа на орене, общее число боев)
coreCont.events.fightResult()
    .on('data', async (event) => {
        let winner = event.returnValues[0],
            looser = event.returnValues[1],
            draw = event.returnValues[2];
        

        //логирование    
        console.log(`бой ${winner} ${looser} ${draw}`);

        //ведение БД для рейтингования
        let fight = await fightModel.save({
            date: new Date,
            firstBirdId: winner,
            secondBirdId: looser,
            draw: draw,
            winner: draw ? null : await coreCont.getUserByBirdId(winner)
        });

        let userWinner = await userModel.getUser(await coreCont.getUserByBirdId(winner)),
            userLooser = await userModel.getUser(await coreCont.getUserByBirdId(looser));

        if (!draw) {
            userWinner.addWinFight(fight._id);
            userLooser.addLooseFight(fight._id);
        }

        //ачивка число побед
        switch (userWinner.win.length){
            case 5:
                userWinner.setAchiv(7);
                break;
            case 15:
                userWinner.setAchiv(8);
                break;
            case 50:
                userWinner.setAchiv(9);
                break;
            case 250:
                userWinner.setAchiv(10);
                break;
            case 500:
                userWinner.setAchiv(11);
                break;
        }

        //ачивка на число боев
        switch (userWinner.win.length + userWinner.loose.length) {
            case 25:
                userWinner.setAchiv(29);
                break;
            case 100:
                userWinner.setAchiv(30);
                break;
            case 500:
                userWinner.setAchiv(31);
                break;
        }

        switch (userLooser.win.length + userLooser.loose.length) {
            case 25:
                userLooser.setAchiv(29);
                break;
            case 100:
                userLooserr.setAchiv(30);
                break;
            case 500:
                userLooser.setAchiv(31);
                break;
        }
    });

coreCont.events.birdLvlUp()
    .on('data', function(event){
        let birdId = event.returnValues[0],
            lvl = event.returnValues[1];

        //ачивка
        switch(lvl){
            case 10:
            
                break;
            case 20: 
                break;
        }
    });

//продажа на бирже   
/*exchCont.events.exchangeSells()
    .on('data', function(event){

    });

//покупка на бирже    
exchCont.events.exchangePurchases()
    .on('data', function(event){

    });

coreCont.events.buyShop()
    .on('data', function(event){

    });*/

//TODO: события по проверки раз в неделю и раз в месяц ТОП N игроков

}
