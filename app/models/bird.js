let names = {
    1:'Ducky',
    2:'Cockmagic',
    3:'Gooosle',
    4:'Smelty Dover',
    5:'Hammer',
    6:'Noisy',
    7:'Hypnocock',
    8:'Deadly boy',
    9:'Fatty Daddy',
    10:'Miss Cuckoo',
    11:'Smartass',
    12:'Blackswan',
    13:'Speedfork',
    14:'Deathfromthenest',
    15:'Brandon Lee',
    16:'Pinky',
    17:'Butcher',
    18:'Captain Sparrow',
    19:'Great snot',
    20:'Big Boss',
    21:'Banana',
    22:'Groundhead',
    23:`Mom's pretty`,
    24:'Just toucan',
    25:'SWAG Bird',
    26:'Red Bag',
    27:'Loveme!',
    28:'Black&White',
    29:'Rainbow',
    30:'Scooter'
}

exports.getNameByType = (_type) => {
    return names[_type];
}