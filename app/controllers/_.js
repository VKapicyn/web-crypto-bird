let dir = __dirname;

module.exports = {
    achiv: require(`${dir}/achiv`).getAchiv,
    birdspedia: require(`${dir}/birdspedia`).getBirdsPedia,
    lend: require(`${dir}/lend`).getLend,
    lk: require(`${dir}/main`).getLK,
    shop: require(`${dir}/shop`).getShop,
    connect: require(`${dir}/connect`).getConnectPage,
    basket: require(`${dir}/basket`),
    helper: require(`${dir}/helper`),
    inventory: require(`${dir}/inventory`)
};