const eth = require('./../models/eth');

exports.getShop = async (req, res) => {
    const userAddress = req.cookies.address,
        userData = await eth.getUserData(userAddress),
        resData = {
            name: userData[1]
        },
        userBalance = await eth.getUserBalance(userAddress),
        orders = await eth.getOrder();
    
    switch(req.query.sortby) {
        case 'lvl-up':
            orders.sort((a, b) => {
                if (a.bird.lvl > b.bird.lvl) return 1;
                return -1;
            });

            break;

        case 'lvl-down':
            orders.sort((a, b) => {
                if (a.bird.lvl > b.bird.lvl) return -1;
                return 1;
            });

            break;

        case 'price-up':
            orders.sort((a, b) => {
                if (a.price > b.price) return 1;
                return -1;
            });

            break;

        case 'price-down':
            orders.sort((a, b) => {
                if (a.price > b.price) return -1;
                return 1;
            });

            break;
    }

    res.render('buy.html', {
        userAddress,
        userData: resData,
        userBalance: userBalance.substring(0, 10),
        orders,
        sort: req.query.sortby
    });
}