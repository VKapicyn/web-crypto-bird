const eth = require('./../models/eth');
const userModel = require('../models/user').userModel;

exports.getAchiv = async (req, res) => {
    const userAddress = req.cookies.address,
    userData = await eth.getUserData(userAddress),
    resData = {
        name: userData[1]
    },
    userBalance = await eth.getUserBalance(userAddress);

    let openAchievs = await userModel.getAchives(userAddress),
        achives = [],
        empties = [];
    
        
    openAchievs.sort((a,b) => {
        return a-b
    });

    for(let i=1; i<31; i++){
        let flag = false;
        for (let j=0; j<openAchievs.length; j++){
            if (openAchievs[j] == i) {
                achives.push({
                    id: i,
                    text: require('../models/achivs').getTextByType(i)
                });
                flag = true;
            }
        }
        if (!flag){
            empties.push({
                id: i,
                text: require('../models/achivs').getTextByType(i)
            });
        }
    }

    res.render('achievs.html', {
        userBalance: userBalance.substring(0, 10),
        userData: resData,
        achives,
        empties
    });
}