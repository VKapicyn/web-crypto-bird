const eth = require('./../models/eth');

exports.getBasketPage = async (req, res) => {
    const userAddress = req.cookies.address,
        userData = await eth.getUserData(userAddress),
        resData = {
            name: userData[1]
        },
        userBalance = await eth.getUserBalance(userAddress);

        let busketPrices = await eth.getBusketPrice();
    
    res.render('basket.html', {
        userAddress,
        busketPrices,
        userData: resData,
        userBalance: userBalance.substring(0, 10),
    });
}