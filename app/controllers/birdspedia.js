const eth = require('./../models/eth');

exports.getBirdsPedia = async (req, res) => {
    const userAddress = req.cookies.address,
    userData = await eth.getUserData(userAddress),
    resData = {
        name: userData[1]
    },
    userBalance = await eth.getUserBalance(userAddress);

    res.render('birdspedia.html', {
        userBalance: userBalance.substring(0, 10),
        userData: resData
    });
}