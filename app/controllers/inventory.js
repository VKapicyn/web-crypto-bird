const eth = require('./../models/eth');

exports.getInventoryPage = async (req, res) => {
    const userAddress = req.cookies.address;

    if (userAddress != undefined) {
        let userData = await eth.getUserData(userAddress),
            resData = {
                name: userData[1]
            },
            userBirds = await eth.getUserBird(userAddress),
            userEquipment = await eth.getUserEquipment(userAddress),
            userBalance = await eth.getUserBalance(userAddress),
            userInventory = await eth.getUserInventory(userAddress),

            buskets = await eth.getBuckets(userAddress);
        
        switch(req.query.sortby) {
            case 'lvl-up':
                userBirds.sort((a, b) => {
                    if (a.level > b.level) return 1;
                    return -1;
                });
    
                break;
    
            case 'lvl-down':
                userBirds.sort((a, b) => {
                    if (a.level > b.level) return -1;
                    return 1;
                });
    
                break;
        }

        res.render('inventory.html', {
            userAddress,
            userData: resData,
            userBirds,
            userBalance: userBalance.substring(0, 10),
            
            buskets,

            userInventory,
            userEquipment
        });
    }
    else
        res.redirect('/user');
}