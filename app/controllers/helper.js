const config = require('../../config');

exports.getConfig = async (req, res) => {
    const response = {
        coreCont: config.coreCont,
        coreABI: config.coreABI,
        exchABI: config.exchABI,
        exchCont: config.exchCont,
        ethNet: config.ethNet
    }

    res.json(response);
}