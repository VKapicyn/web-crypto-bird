const eth = require('./../models/eth');
const fightModel = require('./../models/fights').fightModel;
const userModel = require('../models/user').userModel;
const _bird = require('../models/bird');

exports.getLK = async (req, res) => {
    if (!req.cookies.address) {
        req.cookies.address = '0x0000000000000000000000000000000000000000';
    }

    const userAddress = req.cookies.address;
        userData = await eth.getUserData(userAddress),
        resData = {
            name: userData[1]
        };
        let user = await userModel.getUser(userAddress),
            userLastFights = await user.getLastFights(5);
            userBirds = await eth.getUserBird(userAddress),
            userEquip = await eth.getUserEquipment(userAddress),
            userBalance = await eth.getUserBalance(userAddress),
            liders = await fightModel.getTopUsers(10, 7);

    res.render('index.html', {
        userAddress,
        userData: resData,
        userBalance: userBalance.substring(0, 10),
        userBirds,
        userEquip,
        liders,
        userLastFights,
    });
}