const _ = require('./app/controllers/_');

let express = require('express'),
    router = express.Router();

router.get('/', _.lend);
router.get('/connect', _.connect);
router.get('/achiv', _.achiv);
router.get('/birdspedia', _.birdspedia);
router.get('/user', _.lk);
router.get('/shop', _.shop);
router.get('/baskets', _.basket.getBasketPage);
router.get('/inventory', _.inventory.getInventoryPage);

router.get('/getconfig', _.helper.getConfig);

module.exports = router;