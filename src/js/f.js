
$(document).ready(function(){   

  $('.card-holder').bind('click',function(){
    $(this).toggleClass('js-flip');
  })    
  $('html').bind('click',function(){
    $('.card-holder').removeClass('js-flip');
  })
  $('.card-box, .reverse-card button').bind('click',function(e){
    e.stopPropagation();
  })


  var swiper = new Swiper('.pets-card .swiper-container', {
	  effect: 'coverflow',
	  grabCursor: true,
	  centeredSlides: true,
	  loop: true,
    loopFillGroupWithBlank: true,
	  slidesPerView: 'auto',
	  coverflowEffect: {
	    rotate: 1,
	    stretch: 0,
	    depth: 100,
	    modifier: 1,
	    slideShadows: true,
	  },
	  navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },	  
	});  		

	var swiper = new Swiper('.basket-slider', {
    slidesPerView: 3,
    spaceBetween: 30,
    navigation: {
        nextEl: '.basket-next',
        prevEl: '.basket-prev',
      },
    breakpoints: {      
      640: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      400:{
        slidesPerView: 1,
        spaceBetween: 10,
      }
    }    
  });

  if ($(window).width() < 768) {      
  	var swiper = new Swiper('.card-list .swiper-container', {
    slidesPerView: 1,
    spaceBetween: 0,        
    observer: true,
    observeParents: true,
    navigation: {
        nextEl: '.basket-next',
        prevEl: '.basket-prev',
      },
  });	
	}

  $('.pedia-list, .pedia-txt').niceScroll({
    cursorcolor:"white",
    cursorwidth:"8px",
    cursoropacitymin: 1,
    cursorborder: "1px solid #d1d1d1"
  });

});