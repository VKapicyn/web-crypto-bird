$('#start').on('click', async (e) => {
    await checkMetamask(() => {
        coreContract.getUserDataByAddress(web3.eth.accounts[0], (err, res) => {
            if (res[0] === '') openModal(e);
            else {
                setCookie('address', web3.eth.accounts[0]);
                window.location.replace('/user');
            }
        });
    });
});

$('#modal_close, #overlay').on('click', () => {
    closeModal();
});

function openModal(event) {
    event.preventDefault();
    $('#overlay').fadeIn(400,
        function(){
            $('#reg') 
                .css('display', 'block')
                .animate({opacity: 1, top: '50%'}, 200);
    });

    $('#registrate').on('click', () => {
        const nickname = $('#nickname').val(),
            email = $('#email').val(),
            refer = $('#refer').val();

        coreContract.regUser(nickname, email, refer, {
            from: web3.eth.accounts[0],
            gas: 200000
        }, (err, tx) => {
            setCookie('address', web3.eth.accounts[0]);
            window.location.replace('/user');
            console.log(tx);
        });
    });
}

function closeModal() {
    $('#reg')
        .animate({opacity: 0, top: '45%'}, 200,
            function(){
                $(this).css('display', 'none');
                $('#overlay').fadeOut(400);
            }
        );
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires =    options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
        updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}