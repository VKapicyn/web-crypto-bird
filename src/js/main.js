// SHOP
$('#buy-bronze').on('click', () => {
    buyBasket(1);
});
$('#buy-silver').on('click', () => {
    buyBasket(2);
});
$('#buy-gold').on('click', () => {
    buyBasket(3);
});

//INVENTORY
$('.reverse-card .feed').on('click', (e) => {
    let birdId = e.target.parentNode.dataset.id;
    feedBird(birdId, 1);
})

$('.reverse-card .sell').on('click', (e) => {
    let birdId = e.target.parentNode.dataset.id;
    openModal(e);

    $('#submit-price').on('click', () => {
        let birdPrice = $('#price-input').val();
        sellBird(birdId, birdPrice);
    });
});

$('.reverse-card .kick').on('click', (e) => {
    let birdId = e.target.parentNode.dataset.id;

    burnBird(birdId);
});

$('#modal_close, #overlay').on('click', () => {
    closeModal();
});


$('.buy-bird-btn').on('click', (e) => {
    buyBird(e.target.dataset.id, web3.toWei(e.target.dataset.price, 'ether'));
});

$('#open-bronze').on('click', (e) => {
    openBasket(1);
});
$('#open-silver').on('click', (e) => {
    openBasket(2);
});
$('#open-gold').on('click', (e) => {
    openBasket(3);
});

$('#buy-potion').on('click', (e) => {
    buyPotion();
});

$('#upgrade-inventory').on('click', (e) => {
    upgrInventory();
});

$('#burn-potion').on('click', () => {
    burn(4);
});

$('#burn-eat').on('click', () => {
    burn(0);
});

$('#birdAttack').on('click', () => {
    sendArena();
})

//SHOP
$('.card-item .open-popup').on('click', (e) => {
    let target = e.target.closest('.open-popup');

    $('#modal .card').html(target.querySelector('.card').innerHTML);

    $('#modal .card-name').html(target.querySelector('.name').innerHTML);
    $('#modal .card-lvl').html(target.querySelector('.level').innerHTML);

    let birdId = target.parentNode.querySelector('.buy-btn').dataset.id,
        owner;

    $('#modal .card-id').html("id" + birdId);
    coreContract.getUserByBirdId(birdId, (err, userId) => {
        coreContract.getUserDataByAddress(userId, (err, userData) => {
            owner = userData[1];
            $('#modal .owner').html(owner);
        });
    });
});

function openModal(event) {
    event.preventDefault();
    $('#overlay').fadeIn(400,
        function(){
            $('#enter_price') 
                .css('display', 'block')
                .animate({opacity: 1, top: '50%'}, 200);
    });
}

function closeModal() {
    $('#enter_price')
        .animate({opacity: 0, top: '45%'}, 200,  // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
            function(){ // пoсле aнимaции
                $(this).css('display', 'none'); // делaем ему display: none;
                $('#overlay').fadeOut(400); // скрывaем пoдлoжку
            }
        );
}

async function sendArena() {
    let birdId = $('#birdsList').val(),
        equipId = $('#equipList').val();
    equipId = equipId == null ? 0 : equipId;

    coreContract.findFighter(birdId, equipId, {
        from: web3.eth.accounts[0],
        gas: 500000
    }, (err, tx) => {
        window.location.replace('/user');
        console.log(tx);
    });
    
}

async function buyBasket (type) {
    coreContract.getBuscketPrice((err, price)=>{
        switch(type){
            case 1: 
                price = price[0]/1000000000000000000;
                break;
            case 2: 
                price = price[1]/1000000000000000000;
                break;
            case 3: 
                price = price[2]/1000000000000000000;
                break;
        } 
        const basketPrice = web3.toWei(price, 'ether');

        coreContract.buyBasket(type, {
            from: web3.eth.accounts[0],
            value: basketPrice,
            gas: 200000
        }, (err, tx) => {
            window.location.replace('/inventory');
            console.log(tx);
        });
    });
}

function openBasket(type) {
    coreContract.openBasket(type,{
        from: web3.eth.accounts[0],
        gas: 400000
    }, (err, tx) => {
        console.log(tx);
        window.location.replace('/inventory');
    });
}

function buyPotion() {
    const potionPrice = web3.toWei(0.5, 'ether');

    coreContract.buyPotion({
        from: web3.eth.accounts[0],
        value: potionPrice,
        gas: 200000
    }, (err, tx) => {
        window.location.reload();
        console.log(tx);
    });
}

function upgrInventory() {
    const uprgInvPrice = web3.toWei(0.5, 'ether');

    coreContract.upgradeInventory({
        from: web3.eth.accounts[0],
        value: uprgInvPrice,
        gas: 200000
    }, (err, tx) => {
        window.location.reload();
        console.log(tx);
    });
}

function feedBird(birdId, count) {
    coreContract.feedBird(birdId, count, {
        from: web3.eth.accounts[0],
        gas: 200000
    }, (err, tx) => {
        console.log(tx);
        window.location.reload();
    });
}

function sellBird(birdId, price) {
    exchContract.addNewOrder(0, birdId, price, {
        from: web3.eth.accounts[0],
        gas: 300000
    }, (err, tx) => {
        console.log(tx);
    });
}

function sellEquip(equipId, price) {
    exchContract.addNewOrder(1, equipId, price, {
        from: web3.eth.accounts[0],
        gas: 300000
    }, (err, tx) => {
        console.log(tx);
    });
}

function closeBirdSell(birdId) {
    exchContract.closeOrder(0, birdId, {
        from: web3.eth.accounts[0],
        gas: 200000
    }, (err, tx) => {
        console.log(tx);
    });
}

function closeEquipSell(equipId) {
    exchContract.closeOrder(1, equipId, {
        from: web3.eth.accounts[0],
        gas: 200000
    }, (err, tx) => {
        console.log(tx);
    });
}

function buyBird(birdId, price) {
    exchContract.acceptBirdOrder(birdId, {
        from: web3.eth.accounts[0],
        gas: 300000,
        value: price
    }, (err, tx) => {
        console.log(tx);
    });
}

function burn(type) {
    coreContract.burn(type, {
        from: web3.eth.accounts[0],
        gas: 30000,
    }, (err, tx) => {
        window.location.reload();
        console.log(tx);
    });
}

function burnBird(birdId) {
    coreContract.burnBird(birdId, {
        from: web3.eth.accounts[0],
        gas: 90000
    }, (err, tx) => {
        window.location.reload();
        console.log(tx);
    })
}

function burnEquip(equipId) {
    coreContract.burnEquip(equipId, {
        from: web3.eth.accounts[0],
        gas: 90000
    }, (err, tx) => {
        window.location.reload();
        console.log(tx);
    })
}