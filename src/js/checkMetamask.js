let config;

window.addEventListener('load', async () => {
    config = await $.ajax({
        url: '/getconfig',
        method: 'GET'
    });

    if (window.location.pathname != '/') {
        await checkMetamask();
    }
});

async function checkMetamask(cb) {
    if (typeof web3 !== 'undefined') {
        console.log("Web3 detected!");
        
        web3.version.getNetwork(async (err, netId) => {
            if (netId != config.ethNet) {
                window.location.replace('/connect');
                return;
            }

            window.web3 = new Web3(web3.currentProvider);

            await startApp(cb);
        });
    } else {
        window.location.replace('/connect');
    }
}

async function startApp (cb) {
    let coreABI, coreAddress, exchABI, exchAddress;

    coreABI = config.coreABI;
    coreAddress = config.coreCont;

    exchABI = config.exchABI;
    exchAddress = config.exchCont;

    window.coreContract = web3.eth.contract(coreABI).at(coreAddress);
    window.exchContract = web3.eth.contract(exchABI).at(exchAddress);

    cb();
}